[![pipeline status](https://gitlab.com/thiernotd/spacex-launch/badges/master/pipeline.svg)](https://gitlab.com/thiernotd/movie-search-app/-/commits/master)
# The Movie Search App


You can see the project result [SpaceX Launch](https://spacexvuejs.web.app/).

## Available Scripts

In the project directory, you can run:

### `npm ci`

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.